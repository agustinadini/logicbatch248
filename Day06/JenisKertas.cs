﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class JenisKertas
    {
        public static void Resolve()
        {
            //input: jenis kertas (misal: A5)
            //output: Banyak A6 yang dibutuhkan 2

            Console.WriteLine("Jenis kertas: A6, A5, A4, A3, A2, A1, A0");
            Console.WriteLine("Pilih jenis kertas ");
            string jenisKertas = Console.ReadLine().ToUpper();

            string[] cetakArray = new string[] { "A6", "A5", "A4", "A3", "A2", "A1", "A0" };

            double banyakCetak = 0;
            for (int i = 0; i < cetakArray.Length; i++)
            {
                if (jenisKertas == cetakArray[i])
                {
                    banyakCetak = Math.Pow(2, i);
                }
            }
            Console.WriteLine("Banyak A6 yang dibutuhkan: " + banyakCetak);
        }
    }
}
