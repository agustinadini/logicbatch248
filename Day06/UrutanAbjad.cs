﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class UrutanAbjad
    {
        public static void Resolve()
        {
            //problem: urutkan huruf dari gabungan beberapa kata atau kalimat sesuai abjad alfabet
            //constraint: mengandung huruf vokal &  konsonan, urut dan pisahkan huruf ke dalam 2 kelompok (vokal, konsonan), pisahkan anatara vokal dan konsonan, pakai huruf kecil
            //input: sample case
            //output: huruf vokal = aaee    huruf konsonan: clmpss

            Console.WriteLine("Masukan kalimat");
            string kalimat = Console.ReadLine().ToLower();

            char[] kalimatChar = kalimat.ToCharArray();

            string cekKalimat = "abcdefghijklmnopqrstuvwxyz";
            char[] cekKalimatArray = cekKalimat.ToCharArray();

            string vokal = " ";
            string konsonan = " ";

            for (int i = 0; i < cekKalimatArray.Length; i++)
            {
                for (int j = 0; j < kalimatChar.Length; j++)
                {
                    if (cekKalimatArray[i] == kalimatChar[j])
                    {
                        if (cekKalimatArray[i] == 'a' || cekKalimatArray[i] == 'i' || cekKalimatArray[i] == 'u' || cekKalimatArray[i] == 'e' || cekKalimatArray[i] == 'o')
                        {
                            vokal = vokal + cekKalimatArray[i];
                        }
                        else
                        {
                            konsonan = konsonan + cekKalimatArray[i];
                        }
                    }
                }
            }
            Console.WriteLine("Huruf Vokal : " + vokal);
            Console.WriteLine("Huruf Konsonan : " + konsonan);
        }
    }
}

//point 1 urutkan huruf sesuai alfabet berarti pake ascending
//dipisah jadi 2 tipe yaitu konsonan dan vokal
//proses jadi huruf kecil jadi pakai ToLower