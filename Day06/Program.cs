﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y") //answer to upper kalo user input huruf kecil akan diubah jadi kapital jd gak error
            {
                Console.WriteLine("Masukan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal) //untuk memilih apa inputan user
                {
                    case 1:
                        Console.WriteLine("Soal jenis kertas");
                        JenisKertas.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Soal urutan abjad");
                        UrutanAbjad.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Soal tarif parkir");
                        TarifParkir.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Soal belanja online");
                        BelanjaOnline.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Soal set gambreng");
                        SetGambreng.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Soal set gambreng");
                        coba.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan ");
                        break; //untuk menyelesaikan 
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine(); //kalo user input selain Y maka selesai
            }
        }
    }
}
