﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class BelanjaOnline
    {
        public static void Resolve()
        {
            //pesan online, barang dikirim dalam waktu 7 hari kerja
            //dari info hari dan tanggal pemesanan devi, serta info hari libur nasional
            //maka pada tanggal berapa pesanan devi akan sampai? jika barang tiba bulan berikutnya, tambahkan keterangan (tanggal x di bulan berikutnya)
            //constraint: asumsikan ada 31 hari dalam sebulan, jika dibulan itu tidak ada hari libur nasional tulis 0, bulan berikutnya tdk ada hari libur nasional
            //input: tanggal dan hari pemesanan: 25 Sabtu, hari libur nasional: 26,29
            //output: tanggal 5 di bulan berikutnya

            Console.WriteLine("tanggal dan hari pemesanan");
            string pesan = Console.ReadLine().ToLower();
            string[] SplitPesan = pesan.Split(' ');

            int tanggalPesan = int.Parse(SplitPesan[0]);
            string hariPesan = SplitPesan[1];

            Console.WriteLine("Hari libur Nasional");
            string libur = Console.ReadLine();

            string[] stringLiburArray = libur.Split(' ');
            int[] liburArray = new int[stringLiburArray.Length];

            string[] hariArray = new string[] { "senin", "selasa", "rabu", "kamis", "jumat", "sabtu", "minggu" };

            for (int i = 0; i < liburArray.Length; i++)
            {
                liburArray[i] = Convert.ToInt32(stringLiburArray[i]);
            }

            int indexHari = 0;
            for (int i = 0; i < hariArray.Length; i++)
            {
                if (hariArray[i] == hariPesan)
                {
                    indexHari = i;
                }
            }

            int waktuPengiriman = 7;
            int index = 0;
            string bulan = "";
            int temp = 0;
            while (waktuPengiriman > 0)
            {
                if (indexHari > 6)
                {
                    indexHari = 0;
                }
                if (liburArray[index] == tanggalPesan || index > 4)
                {
                    if (liburArray[index] == tanggalPesan)
                    {
                        if (index < liburArray.Length - 1)
                        {
                            index++;
                        }
                    }

                    if (tanggalPesan == 31)
                    {
                        tanggalPesan = 0;
                        bulan = " Bulan berikutnya";
                    }
                }
                else if (tanggalPesan != liburArray[index] && indexHari < 5)
                {
                    if (tanggalPesan == 31)
                    {
                        tanggalPesan = 0;
                        bulan = " Bulan berikutnya";
                    }
                    temp = tanggalPesan;
                    waktuPengiriman--;
                }
                indexHari++;
                tanggalPesan++;
            }
            Console.WriteLine("Pesanan sampai pada tanggal " + temp + bulan);
        }
    }
}