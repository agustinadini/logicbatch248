﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class TarifParkir
    {
        public static void Resolve()
        {
            //kalkulasi tarif parkir dengan ketentuan
            //8 jam pertama 1000/jam
            //lebih dari 8 jam s.d 24 jam harga 8000 flat
            //lebih dari 24 jam harga 15000/24 jam  dan selebihnya mengikuti ketentuan pertama dan kedua
            //input: tanggal dan jam masuk, tanggal dan jam keluar ==> 28 january 2020 07:30:34, 28 january 2020 20:03:35
            //output: besarnya tarif parkir ==> 8000
            //penjelasan: lamanya parkir adalah 12 jam 33 menit`1 detik, sehingga perhitungan tarif parkir dibulatkan menjadi 13 jam.
            //mengacu pada ketentuan kedua, maka yang harus dibayarkan adlah 8000

            

            Console.WriteLine("Tanggal dan Jam Masuk : ");
            string stringMasuk = Console.ReadLine();

            Console.WriteLine("Tanggal dan Jam Keluar : ");
            string stringKeluar = Console.ReadLine();

            //konversi datetime
            DateTime waktuMasuk = Convert.ToDateTime(stringMasuk);
            DateTime waktuKeluar = Convert.ToDateTime(stringKeluar);

            double selisihHari = (waktuKeluar - waktuMasuk).TotalDays;

            double lamaParkir = selisihHari * (double)24;

            double biaya = 0;

            //validasi minus
            if (lamaParkir <= 8)
            {
                biaya = lamaParkir * 1000;
                biaya = biaya - (biaya % 1000);
            }

            //kurang dari dua jam
            else if (lamaParkir > 8 && lamaParkir < 24)
            {
                biaya = biaya - (biaya % 1000) + 8000;
            }

            //lebih dari dua jam dan kurang dari 24 jam
            else if (lamaParkir > 24)
            {
                double temp = lamaParkir;
                int hari = 0;
                while (temp > 24)
                {
                    temp = Convert.ToInt32(temp) - 24; // tidak mudeng!!!!
                    hari++;
                }

                int parkir24Jam = 15000 * hari;
                double WaktuTersisa = lamaParkir - (hari * 24);

                if (WaktuTersisa <= 8)
                {
                    biaya = WaktuTersisa * 1000;
                    biaya = biaya - (biaya % 1000) + parkir24Jam;
                }
                else if (WaktuTersisa > 8 && WaktuTersisa < 24)
                {
                    biaya = biaya - (biaya % 1000) + 8000 + parkir24Jam;
                }
            }
            Console.WriteLine("Biaya parkir adalah Rp " + biaya);
        }
    }
}
