﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class SetGambreng
    {
        public static void Resolve()
        {
            //length set harus sama
            //urutin atas kebawah 
            //kondisinya G > K, K > B, B > G = 1 jika sama maka seri = 1 else nya kalah =1

            Console.Write("Set Gambreng A: ");
            string gambrengA = Console.ReadLine();

            Console.Write("Set Gambreng B: ");
            string gambrengB = Console.ReadLine();

            char[] gambrengAChar = gambrengA.ToUpper().ToCharArray();
            char[] gambrengBChar = gambrengB.ToUpper().ToCharArray();

            if (gambrengAChar.Length != gambrengBChar.Length)
            {
                Console.WriteLine("Inputan yang anda masukan salah");
            }
            else
            {

                int pemainA = 0;
                int pemainB = 0;

                int seri = 0;

                int menangA = 0;
                int kalahA = 0;

                int menangB = 0;
                int kalahB = 0;

                for (int i = 0; i < gambrengAChar.Length; i++)
                {
                    if (gambrengAChar[i] == gambrengBChar[i])
                    {
                        seri++;
                    }
                    else if (i == i)
                    {
                        if (gambrengAChar[i] == 'G' && gambrengBChar[i] == 'B')
                        {
                            pemainB++;
                            menangB++;
                            kalahA++;
                        }
                        else if (gambrengAChar[i] == 'G' && gambrengBChar[i] == 'K')
                        {
                            pemainA++;
                            menangA++;
                            kalahB++;
                        }
                        else if (gambrengAChar[i] == 'B' && gambrengBChar[i] == 'G')
                        {
                            pemainA++;
                            menangA++;
                            kalahB++;
                        }
                        else if (gambrengAChar[i] == 'B' && gambrengBChar[i] == 'K')
                        {
                            pemainB++;
                            menangB++;
                            kalahA++;
                        }
                        else if (gambrengAChar[i] == 'K' && gambrengBChar[i] == 'G')
                        {
                            pemainB++;
                            menangB++;
                            kalahA++;
                        }
                        else if (gambrengAChar[i] == 'K' && gambrengBChar[i] == 'B')
                        {
                            pemainA++;
                            menangA++;
                            kalahB++;
                        }
                    }
                }
                Console.WriteLine("Pemain A: " + menangA + " Menang " + kalahA + " Kalah " + seri + " Seri ");
                Console.WriteLine("Pemain B: " + menangB + " Menang " + kalahB + " Kalah " + seri + " Seri ");

                Console.WriteLine();

                if (pemainA > pemainB)
                {
                    Console.WriteLine("Pemain A Menang");
                }
                else if (pemainA < pemainB)
                {
                    Console.WriteLine("Pemain B menang");
                }
                else
                {
                    Console.WriteLine("Seri");
                }
            }
        }
    }
}
