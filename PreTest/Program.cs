﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y") //answer to upper kalo user input huruf kecil akan diubah jadi kapital jd gak error
            {
                Console.WriteLine("Masukan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal) //untuk memilih apa inputan user
                {
                    case 1:
                        BeliPulsa.Resolve();
                        break;

                    case 2:
                        Bensin.Resolve();
                        break;

                    case 3:
                        KonversiVolume.Resolve();
                        break;

                    case 4:
                        SaldoOpo.Resolve();
                        break;

                    case 5:
                        NasiGoreng.Resolve();
                        break;

                    case 6:
                        Gamekomputer.Resolve();
                        break;

                    case 7:
                        Keranjang.Resolve();
                        break;

                    case 8:
                        ATM.Resolve();
                        break;

                    case 9:
                        Kartu.Resolve();
                        break;

                    case 10:
                        PrimaFibonacci.Resolve();
                        break;

                    case 11:
                        NumberOne.Resolve();
                        break;

                    case 12:
                        EviternityNumbers.Resolve();
                        break;


                    default:
                        Console.WriteLine("Soal tidak ditemukan ");
                        break; //untuk menyelesaikan 
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine(); //kalo user input selain Y maka selesai
            }
        }
    }
}
