﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class NumberOne
    {
        public static void Resolve ()
        {
            Console.WriteLine("Masukan panjang :");
            int panjang = int.Parse(Console.ReadLine());

            int deretAngka = 100;
            string deretAngka1 = "";

            while (0 < panjang)
            {
                double temp = 0;
                double hasilKuadrat = 0;
                deretAngka1 = Convert.ToString(deretAngka);

                char[] deretanAngka = deretAngka1.ToCharArray();
                hasilKuadrat = 0;

                for (int j = 0; j < deretanAngka.Length; j++)
                {
                    temp = Math.Pow((int)char.GetNumericValue(deretanAngka[j]), 2);
                    hasilKuadrat += temp;
                }
                if (hasilKuadrat >= 0 && hasilKuadrat <= 9)
                {
                    if (hasilKuadrat == 1)
                    {
                        Console.Write(deretAngka + " ");
                        panjang--;
                    }
                }
                else
                {
                    while (hasilKuadrat >= 10)
                    {
                        string tempHasilKuadrat = hasilKuadrat.ToString();
                        char[] tempChar = tempHasilKuadrat.ToCharArray();
                        temp = 0;
                        hasilKuadrat = 0;

                        for (int k = 0; k < tempChar.Length; k++)
                        {
                            temp = Math.Pow((int)char.GetNumericValue(tempChar[k]), 2);
                            hasilKuadrat += temp;
                        }
                        if (hasilKuadrat >= 0 && hasilKuadrat <= 9)
                        {
                            if (hasilKuadrat == 1)
                            {
                                Console.Write(deretAngka + " ");
                                panjang--;
                            }
                        }
                    }
                }
            }
            Console.WriteLine();
        }
    }
}
