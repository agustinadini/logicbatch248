﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class PrimaFibonacci
    {
        public static void Resolve()
        {
            //deret angka yang terbentuk dari penjumlahan deret bilangan fibonacci dan bilangan prima

            Console.WriteLine("Masukan panjang deret: ");
            int panjang = int.Parse(Console.ReadLine());

            int[] primaArray = new int[panjang]; //untuk menyimpan nilai
            int[] fibonacciArray = new int[panjang];
            int[] result = new int[panjang];

            for (int i = 0; i < panjang; i++)
            {
                if (i <= 1)
                {
                    fibonacciArray[i] = 1;
                    Console.Write(fibonacciArray[i] + " " + "\t");
                }
                else
                {
                    fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                    Console.Write(fibonacciArray[i] + " " + "\t");
                }
            }
            Console.WriteLine();

            int bilanganPrima = 2;
            int panjangBilanganPrima = 0;
            
            while (panjangBilanganPrima < panjang)
            {
                int faktorBilanganPrima = 0;
                for (int i = 1; i <= bilanganPrima; i++)
                {
                    if (bilanganPrima % i == 0)
                    {
                        faktorBilanganPrima += 1;
                    }
                }
                if (faktorBilanganPrima == 2)
                {
                    primaArray[panjangBilanganPrima] = bilanganPrima;
                    Console.Write(bilanganPrima + " " + "\t");
                    panjangBilanganPrima++;
                }
                bilanganPrima++;
            }
            Console.WriteLine();

            for (int i = 0; i < panjang; i++)
            {
                result[i] = fibonacciArray[i] + primaArray[i];
                Console.Write(result[i] + " " + "\t");
            }
        }
    }
}

