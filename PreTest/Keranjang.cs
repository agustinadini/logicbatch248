﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Keranjang
    {
        public static void Resolve()
        {
            //ada 3 keranjang di dapur, salah satunya kosong dan 2 berisi buah dengan jumlah n & m buah
            //jika 1 keranjang dibawa ke pasar 
            //berapa jumlah buah yang terdapat di dapur?

            //input
            //keranjang 1 = kosong              //keranjang 1 = 5
            //keranjang 2 = 10                  //keranjang 2 = kosong
            //keranjang 3 = 5                   //keranjang 3 = 5

            //output
            //keranjang 1 dibawa ke pasar       //keranjang 3 dibawa ke pasar
            //jumlah buah adalah 10 + 5        //jumlah buah adalah 5 + kosong = 5

            //diketahui keranjang 1=5, keranjang 2=10, keranjang=5 jadi total buah = 20

            Console.WriteLine("Masukan buah ke dalam keranjang 1: ");
            int keranjang1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan buah ke dalam keranjang 2: ");
            int keranjang2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan buah ke dalam keranjang 3: ");
            int keranjang3 = int.Parse(Console.ReadLine());

            Console.WriteLine("Keranjang mana yang akan kamu bawa? ");
            int bawaKeranjang = int.Parse(Console.ReadLine());

            switch (bawaKeranjang)
            {
                case 1:
                    int bawaKeranjang1 = keranjang2 + keranjang3;
                    Console.WriteLine("Jumlah buah yang ada di dapur adalah " + bawaKeranjang1);
                    break;

                case 2:
                    int bawaKeranjang2 = keranjang1 + keranjang3;
                    Console.WriteLine("Jumlah buah yang ada di dapur adalah " + bawaKeranjang2);
                    break;

                case 3:
                    int bawaKeranjang3 = keranjang1 + keranjang2;
                    Console.WriteLine("Jumlah buah yang ada di dapur adalah " + bawaKeranjang3);
                    break;

                default:
                    Console.WriteLine("Keranjang yang anda masukan salah!");
                    break;
            }
            Console.WriteLine();
        }
    }
}

