﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class EviternityNumbers
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan angka");
            int input = int.Parse(Console.ReadLine());

            string temp = "";
            int cekAngka3 = 0;
            int cekAngka5 = 0;

            for (int i = 0; i < input; i++)
            {
                if (i == 8)
                {
                    Console.Write(i + " ");
                }
                else if (i > 8)
                {
                    temp = Convert.ToString(i);

                    if ((temp.Contains('8') && temp.Contains('5') && !temp.Contains('1') && !temp.Contains('2') && !temp.Contains('4') && !temp.Contains('6') && !temp.Contains('7') && !temp.Contains('9') && !temp.Contains('0')))
                    {
                        char[] angka = temp.ToCharArray();
                        for (int j = 0; j < angka.Length; j++)
                        {
                            if (angka[j] == '3')
                            {
                                cekAngka3++;
                            }
                            if (angka[j] == '5')
                            {
                                cekAngka5++;
                            }
                        }
                        if (cekAngka3 == 0 && cekAngka5 == 1)
                        {
                            Console.Write(i + " ");
                            cekAngka3 = 0;
                            cekAngka5 = 0;
                        }
                        else if (cekAngka3 == 1 && cekAngka5 == 0)
                        {
                            Console.Write(i + " ");
                            cekAngka3 = 0;
                            cekAngka5 = 0;
                        }
                        else if (cekAngka3 == 1 && cekAngka5 == 1)
                        {
                            Console.Write(i + " ");
                            cekAngka3 = 0;
                            cekAngka5 = 0;
                        }
                        else
                        {
                            cekAngka3 = 0;
                            cekAngka5 = 0;
                        }
                    }
                    else if (temp.Contains('8') && !temp.Contains('1') && !temp.Contains('2') && !temp.Contains('3') && !temp.Contains('4') && !temp.Contains('6') && !temp.Contains('7') && !temp.Contains('9') && !temp.Contains('0'))
                    {
                        Console.Write(i + " ");
                    }
                }
            }
        }
    }
}
