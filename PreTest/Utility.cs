﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Utility
    {
        public static int[] ConvertStringToIntArray(string numbers)
        {
            string[] stringNumberArray = numbers.Split(' ');
            int[] numbersArray = new int[stringNumberArray.Length];

            //Convert to int
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = int.Parse(stringNumberArray[i]);

            }

            return numbersArray;
        }

        public static int Sum(int bilanganPrima, int fibonacciArray)
        {
            int total = bilanganPrima + fibonacciArray;
            return total;
        }
    }
}

