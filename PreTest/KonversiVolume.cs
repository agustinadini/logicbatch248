﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class KonversiVolume
    {
        public static void Resolve()
        {
            //konversi volume
            //jika 1 botol = 2 gelas && 1 teko = 25 cangkir && 1 gelas = 2,5 cangkir

            Console.WriteLine("Gelas, Teko, Cangkir, Botol");
            Console.WriteLine("Apa yang kamu pilih?");
            string pilihan = Console.ReadLine();
            string[] splitPilihan = pilihan.Split(' ');

            int jumlahPilihan = Convert.ToInt32(splitPilihan[0]);
            string namaPilihan = splitPilihan[1];

            Console.WriteLine("Dikonversi ke ");
            string konversi = Console.ReadLine().ToLower();

            double temp = 0;
            if (namaPilihan == "Cangkir")
            {
                if (konversi == "Gelas")
                {
                    temp = jumlahPilihan / 2.5;
                }
                else if (konversi == "Botol")
                {
                    temp = jumlahPilihan / 5;
                }
                else if (konversi == "Teko")
                {
                    temp = jumlahPilihan / 25;
                }
            }
            else if (namaPilihan == "Gelas")
            {
                if (konversi == "Botol")
                {
                    temp = jumlahPilihan * 2.5;
                }
                else if (konversi == "Botol")
                {
                    temp = jumlahPilihan / 2;
                }
                else if (konversi == "Teko")
                {
                    temp = jumlahPilihan / 10;
                }
            }
            else if (namaPilihan == "Botol")
            {
                if (konversi == "Cangkir")
                {
                    temp = jumlahPilihan * 5;
                }
                else if (konversi == "Gelas")
                {
                    temp = jumlahPilihan * 2;
                }
                else if (konversi == "'Teko")
                {
                    temp = jumlahPilihan / 5;
                }
            }
            else if (namaPilihan == "Teko")
            {
                if (konversi == "Cangkir")
                {
                    temp = jumlahPilihan * 25;
                }
                else if (konversi == "Gelas")
                {
                    temp = jumlahPilihan * 10;
                }
                else if (konversi == "Botol")
                {
                    temp = jumlahPilihan * 5;
                }
            }
            Console.WriteLine(jumlahPilihan + " " + namaPilihan + "=" + temp + " " + konversi);
        }
    }
}
