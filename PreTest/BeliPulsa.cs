﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class BeliPulsa
    {
        public static void Resolve()
        {
            //Transaksi beli pulsa akan mendapat point dengan ketentuan sebagai berikut
            //0 - 10000 dapet 0 point
            //10001 -30000 dapet point 1 untuk kelipatan 1000
            //>30000 dapet point 2 untuk kelipatan 1000
            //input: 20000      output: 10 point

            Console.WriteLine("Masukan jumlah pulsa: ");
            int jumlahPulsa = int.Parse(Console.ReadLine());

            int point = 0;

            if (jumlahPulsa <= 10000)
            {
                point = 0;
            }
            else if (jumlahPulsa >= 10001 && jumlahPulsa <= 30000)
            {
                point = ((jumlahPulsa - 10000) / 1000) * 1;
            }
            else if (jumlahPulsa > 30000)
            {
                point = ((jumlahPulsa - 20000) / 1000) * 2;
            }
            Console.WriteLine("Point yang didapat adalah " + point + " point");
        }
    }
}
