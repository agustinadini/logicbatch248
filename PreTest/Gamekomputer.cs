﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Gamekomputer
    {
        public static void Resolve()
        {
            Console.WriteLine("Pemain pertama : 1. pemain || 2. Komputer");
            int jawaban = int.Parse(Console.ReadLine());
            if (jawaban == 1)
            {
                Console.WriteLine("Giliran anda");
                int pemain = int.Parse(Console.ReadLine());
                if (pemain > 9 || pemain < 0)
                {
                    Console.WriteLine("Input yang anda masukan salah");
                }
                else
                {
                    Random komputer = new Random();
                    int angkaKomputer = komputer.Next(0, 9);

                    Console.WriteLine("Giliran Komputer");
                    Console.WriteLine(angkaKomputer);

                    if (pemain > angkaKomputer)
                    {
                        Console.WriteLine("Kamu menang");
                    }
                    else
                    {
                        Console.WriteLine("Kamu kalah");
                    }
                }
            }
            else if (jawaban == 2)
            {
                Random komputer = new Random();
                int angkaKomputer = komputer.Next(0, 9);

                Console.WriteLine("Giliran Komputer");
                Console.WriteLine("Rahasia");

                Console.WriteLine();
                Console.WriteLine("Giliran anda");
                int pemain = int.Parse(Console.ReadLine());
                if (pemain > 9 || pemain < 0)
                {
                    Console.WriteLine("Input yang anda masukan salah");
                }
                else
                {

                    if (pemain > angkaKomputer)
                    {
                        Console.WriteLine("You WINN");
                    }
                    else
                    {
                        Console.WriteLine("You LOSSSE");
                    }
                }
            }
            else
            {
                Console.WriteLine("Inputan anda salah");
            }

        }
    }
}
