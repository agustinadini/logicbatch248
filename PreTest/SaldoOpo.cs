﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class SaldoOpo
    {
        public static void Resolve()
        {
            Console.WriteLine("Saldo OPO:");
            int saldo = int.Parse(Console.ReadLine());

            double diskon = 0.5;
            double cashbackPercent = 0.1;
            double temp = saldo;
            int cup = 0;
            double totalBelanja = 0;
            double cashback = 0;

            if (saldo < 40000)
            {
                while (temp < 40000 && temp >= 18000)
                {
                    if (temp >= 18000)
                    {
                        temp -= 18000;
                        cup++;
                    }
                }

                cashback = saldo - temp;
                cashback = cashback * cashbackPercent;
                temp += cashback;
                Console.WriteLine("Jumlah Cup = " + cup);
                Console.WriteLine("Saldo Akhir = " + temp);

            }
            else
            {
                while (temp > (18000 * diskon))
                {
                    temp -= (18000 * diskon);
                    cup++;
                }

                totalBelanja = saldo - temp;
                if (totalBelanja > 100000)
                {
                    Console.WriteLine("Tidak jadi membeli");
                }

                else
                {
                    cashback = totalBelanja * cashbackPercent;
                    if (cashback > 30000)
                    {
                        cashback = 30000;
                    }
                    temp += cashback;
                    Console.WriteLine("Jumlah Cup = " + cup);
                    Console.WriteLine("Saldo Akhir = " + temp);
                }

            }
        }
    }
}
