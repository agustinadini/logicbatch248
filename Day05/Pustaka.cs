﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Pustaka
    {
        public static void Resolve()
        {
            //denda kali 5000 per hari per buku
            //lama peminjaman 3 hari

            //input : berapa buku = tanggal peminjaman = tanggal pengembalian
            //output : denda = 30.000

            Console.WriteLine("Input buku yang dipinjam: ");
            int buku = int.Parse(Console.ReadLine());

            Console.WriteLine("Input tanggal peminjaman: ");
            string waktuPeminjamanString = Console.ReadLine();

            Console.WriteLine("Input tanggal pengembalian");
            string waktupengembalianString = Console.ReadLine();

            DateTime waktuPeminjaman = Convert.ToDateTime(waktuPeminjamanString);
            DateTime waktuPengembalian = Convert.ToDateTime(waktupengembalianString);

            int lamaPeminjaman = (waktuPengembalian - waktuPeminjaman).Days;
            int jumlahDenda = ((buku * lamaPeminjaman) * 5000) - (15000 * buku);

            if (lamaPeminjaman > 3)
            {
                Console.WriteLine(jumlahDenda);
            }
            else if (lamaPeminjaman < 0)
            {
                Console.WriteLine("Salah input!");
            }
            else
            {
                Console.WriteLine("Tidak ada denda");
            }
        }
    }
}