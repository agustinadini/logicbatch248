﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class customSort
    {
        public static void Resolve()
        {
            //soal : urutkan angka dari yang terkecil ke terbesar
            //input : 2 1 3 5 2 1 4
            //output : 1 1 2 2 3 4 5

            Console.WriteLine("Masukan angka yang akan diurutkan");
            string angka = Console.ReadLine();

            int[] angkaArray = Utility.ConvertStringToIntArray(angka);

            int tukar;

            //perulangan untuk geser angka terkecil ke paling kiri
            for (int i = 0; i < angkaArray.Length; i++)
            {
                //perulangan untuk sorting dari angka terkecil ke terbesar
                for (int j = 1; j < angkaArray.Length; j++)
                {
                    //kondisi untuk sorting descending
                    if (angkaArray[j] < angkaArray[j - 1])
                    {
                        tukar = angkaArray[j - 1];
                        angkaArray[j - 1] = angkaArray[j];
                        angkaArray[j] = tukar;

                    }
                }
            }

            //perulangan untuk menampilkan hasil sorting
            for (int i = 0; i < angkaArray.Length; i++)
            {
                Console.Write(angkaArray[i] + " ");
            }
            Console.WriteLine();
        }
    }
}
