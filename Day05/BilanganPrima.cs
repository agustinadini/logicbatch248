﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class BilanganPrima
    {
        public static void Resolve()
        {
            //soal n = 7
            // output = 2 3 5 7 11 13 17

            Console.WriteLine("Masukkan panjang n");
            int panjang = int.Parse(Console.ReadLine());

            int bilanganPrima = 2;
            int panjangBilanganPrima = 0;

            //while jagain supaya panjangnya tidak lebih dari panjang inputan
            while (panjangBilanganPrima < panjang) 
            {
                int faktorBilanganPrima = 0;

                //untuk mencari bilangan prima
                for (int i = 1; i <= bilanganPrima; i++)
                {
                    if (bilanganPrima % i == 0)
                    {
                        faktorBilanganPrima += 1;
                    }
                }

                //untuk cetak bilangan prima
                if (faktorBilanganPrima == 2) // ==2 karena bilangan prima itu hanya bisa dibagi dengan 1 dan bilangan itu sendiri
                {
                    Console.Write(bilanganPrima + " ");
                    panjangBilanganPrima++;
                }
                bilanganPrima++;
            }
            Console.WriteLine();
        }
    }
}





// the idea
// user dapat menginputkan length, setelah itu bilangan primanya muncul sesuai panjang length
// bilangan prima adalah bilangan yang hanya bisa dibagi oleh 1 dan bilangan itu sendiri
//pas diinputkan 7 terus muncul bilangan primanya sepanjang 7 baris
// kalo i <= 1 maka bukan bilangan prima
// kalo i > 2 dan hasil pembagiannya 0 maka bukan bilangan prima
//berarti cara ngitung bilangan primanya adalah  

//cari codingan menghitung bilangan prima xxx % j