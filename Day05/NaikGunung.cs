﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class NaikGunung
    {
        public static void Resolve()
        {
            //Input = UUDDUDUDDUU U= up D= down
            //Output = naik: 4 turun 1

            Console.WriteLine("Masukan kode naik turun gunung tanpa spasi: ");
            string kode = Console.ReadLine().ToUpper();

            char[] charArray = kode.ToCharArray();

            int naikUp = 0;
            int turunDown = 0;
            int jalurPendakian = 0;

            for (int i = 0; i < charArray.Length; i++)
            {
                if (charArray[i] == 'U' || charArray[i] == 'N')
                {
                    jalurPendakian += 1;

                    if(jalurPendakian == 1)
                    {
                        naikUp += 1;
                    }
                }
                else if (charArray[i] == 'D' || charArray[i] == 'T')
                {
                    jalurPendakian -= 1;

                    if(jalurPendakian == -1)
                    {
                        turunDown += 1;
                    }
                }
            }
            Console.WriteLine("Posisi terakhir pendakian: " + jalurPendakian);
            Console.WriteLine("Jumlah naik: " + naikUp);
            Console.WriteLine("Jumlah turun " + turunDown);
        }
        
    }
}
