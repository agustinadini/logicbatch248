﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class KaosKaki
    {
        public static void Resolve()
        {
            // input = 1 2 1 3 3 3 1 1 2 4
            // output = 4 (pasang)

            Console.WriteLine("Masukan deret angka kaos kaki");
            string kaosKaki = Console.ReadLine();

            if (!kaosKaki.Contains(' ') || kaosKaki.Contains('-'))
            {
                Console.WriteLine("Inputan yang anda masukan tidak memiliki pasangan!");
            }
            else
            {
                //ubah string ke int
                int[] angkaArray = Utility.ConvertStringToIntArray(kaosKaki);

                int sepasang = 0;

                //sort dulu inputannya
                Array.Sort(angkaArray);

                //mencari pasangan kaoskaki
                for (int i = 0; i < angkaArray.Length - 1; i++) //-1 untuk mengecek nilai sebelumnya
                {
                    if (angkaArray[i] == angkaArray[i + 1])
                    {
                        sepasang++;
                        i++;
                    }
                }
                Console.WriteLine("Total Pasangan KaosKaki: " + sepasang);
            }

            
        }
    }
}
