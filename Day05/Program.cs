﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y") //answer to upper kalo user input huruf kecil akan diubah jadi kapital jd gak error
            {
                Console.WriteLine("Masukan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal) //untuk memilih apa inputan user
                {
                    case 1:
                        Console.WriteLine("Soal bilangan prima");
                        BilanganPrima.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Soal es loli");
                        EsLoli.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Soal modus");
                        Modus.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Soal custom sort");
                        customSort.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Soal pustaka");
                        Pustaka.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Soal Kacamata dan Baju");
                        KacamataDanBaju.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("Soal lilin fibonacci");
                        LilinFibonacci.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Soal integer geser");
                        IntegerGeser.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("Soal Parkir");
                        Parkir.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Soal naik gunung");
                        NaikGunung.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("Soal Kaos Kaki");
                        KaosKaki.Resolve();
                        break;

                    case 12:
                        Console.WriteLine("Soal Pembulatan Nilai");
                        PembulatanNilai.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan ");
                        break; //untuk menyelesaikan 
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine(); //kalo user input selain Y maka selesai
            }

        }
    }
}
