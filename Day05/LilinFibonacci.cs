﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class LilinFibonacci
    {
        public static void Resolve()
        {
            //input: 5 5 7 9
            //output: lilin ke 4

            Console.WriteLine("Enter the set number");
            string number = Console.ReadLine();

            int[] numberArray = Utility.ConvertStringToIntArray(number);

            int index = 0;
            bool isNegative = false;

            while (index < numberArray.Length)
            {
                if (numberArray[index] < 0)
                {
                    isNegative = true;
                    break;
                }
                index++;
            }
            if (isNegative == false)
            {
                int[] fibonacciArray = new int[numberArray.Length];
                double[] waktuHabisLilin = new double[numberArray.Length];

                //input angka fibonacci ke array fibonacciArray

                for (int i = 0; i < numberArray.Length; i++)
                {
                    if (i <= 1)
                    {
                        fibonacciArray[i] = 1;
                        Console.Write(fibonacciArray[i] + " ");
                    }
                    else
                    {
                        fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                        Console.Write(fibonacciArray[i] + " ");
                    }
                }
                Console.WriteLine();

                //menghitung waktu lilin habis saat dibakar
                for (int j = 0; j < numberArray.Length; j++)
                {
                    waktuHabisLilin[j] = (double)numberArray[j] / fibonacciArray[j];
                    Console.Write(waktuHabisLilin[j] + " ");
                }
                Console.WriteLine();

                //mencetak lilin yang paling cepat habis
                for (int k = 0; k < numberArray.Length; k++)
                {
                    if (waktuHabisLilin[k] == waktuHabisLilin.Min())
                    {
                        Console.WriteLine("Lilin yang cepat habis adalah lilin ke " + (k + 1));
                    }
                    
                }
                Console.WriteLine();
            }
        }
    }
}




  