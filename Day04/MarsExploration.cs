﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class MarsExploration
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the text");
            string text = Console.ReadLine().ToUpper();

            int countS = 0;
            int countO = 0;

            if (text.Length % 3 != 0)
            {
                Console.WriteLine("Incorrect SOS entered");
            }
            else
            {
                for (int i = 0; i < text.Length; i++)
                {
                    if (i % 3 == 0 || i % 3 == 2)
                    {
                        if (text[i] != 'S')
                        {
                            countS++;
                        }
                    }
                    if (i % 3 == 1)
                    {
                        if (text[i] != 'O')
                        {
                            countO++;
                        }
                    }
                }
                int countTotal = countS + countO;
                Console.WriteLine(countTotal);
            }
        }
    }
}
