﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class TwoString
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the text");
            string text = Console.ReadLine();

            Console.WriteLine("Enter the text again");
            string text2 = Console.ReadLine();

            char[] textChar = text.ToCharArray();
            char[] textChar2 = text2.ToCharArray();

            Boolean condition = false;

            for (int i = 0; i < textChar.Length; i++)
            {
                for (int j = 0; j < textChar2.Length; j++)
                {
                    if (textChar[i] == (textChar2[j])) //equals itu sama aja =
                    {
                        condition = true;
                        break;
                    }
                }
            }

            if (condition)
            {
                Console.WriteLine("YES");
            }
            else
            {
                Console.WriteLine("NO");
            }
        }
    }
}
