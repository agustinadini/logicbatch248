﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Pangrams
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the text");
            string text = Console.ReadLine().ToLower();
            char[] textChar = text.ToCharArray();

            string cek = "abcdefghijklmnopqrstuvwxyz ";
            char[] cekCar = cek.ToCharArray();

            int cekBantu = 0;

            for (int i = 0; i < cekCar.Length; i++) //cekCar.Length panjang karakter
            {
                for (int j = 0; j < text.Length; j++)
                {
                    if (cekCar[i] == textChar[j])
                    {
                        cekBantu++;
                        break;
                    }
                }
            }

            if (cekBantu == cekCar.Length)
            {
                Console.WriteLine("Pangrams");
            }
            else
            {
                Console.WriteLine("Bukan Pangrams");
            }
        }
    }
}
