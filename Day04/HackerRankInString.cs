﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class HackerRankInString
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Enter the text you want");
            string stringPembanding = Console.ReadLine();
            char[] charPembanding = stringPembanding.ToCharArray();

            Console.WriteLine("Enter the total text: ");
            int count = int.Parse(Console.ReadLine());
            string[] textArray = new string[count];

            //input kalimat berdasar jumlah inputan
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine("Input text " + (i + 1));
                textArray[i] = Console.ReadLine().ToLower();
            }

            //bandingkan dengan urutan karakter string pembanding "haccerrank" dengan urutan karakter kalimat yang diinput
            for (int i = 0; i < textArray.Length; i++)
            {
                //variabel nilai pembanding untuk mengeset nilai pembanding dengan awalnya 0
                int countPembanding = 0;
                char[] textArrayChar = textArray[i].ToCharArray();

                //perulangan untuk membandingkan urutan string kalimat inputan dengan urutan char pembanding "haccerrank" berdasar urutannya
                for (int j = 0; j < textArrayChar.Length; j++)
                {
                    //kondisi pengecekan urutan string kalimat inputan dengan urutan char pembanding "haccerrank"
                    //apabila sesuai urutan terdapat kesamaan maka nilai pembanding akan bertambah
                    if (countPembanding < charPembanding.Length)
                    {
                        if (textArrayChar[j] == charPembanding[countPembanding])
                        {
                            countPembanding++;
                        }
                    }
                }

                //karena jumlah karakter pembanding "haccerank" = 10 maka jumlahnya harus 10
                //apabila jumlahnya "10" maka cetak output YES dan apabila jumlahnya selain "10" maka cetak output "NO"
                if (countPembanding == charPembanding.Length)
                {
                    Console.WriteLine();
                    Console.WriteLine("Kalimat ke - " + (i + 1) + " is a Yes");
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Kalimat ke- " + (i + 1) + " is a No");
                }
            }
        }
    }
}
