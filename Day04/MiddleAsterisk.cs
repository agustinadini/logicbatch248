﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class MiddleAsterisk
    {
        public static void Resolve()
        {
            Console.WriteLine();
            Console.WriteLine("Enter the text");
            string text = Console.ReadLine();
            string[] asteriskArray = text.Split(' '); //split untuk mengisi kekosongan

            string result = " ";

            for (int i = 0; i < asteriskArray.Length; i++)
            {
                char[] asteriskArrayChar = asteriskArray[i].ToCharArray();

                for (int j = 0; j < asteriskArrayChar.Length; j++)
                {
                    if (j == 0 || j == asteriskArrayChar.Length - 1)
                    {
                        result += asteriskArrayChar[j].ToString();
                    }
                    else
                    {
                        result += " ";
                    }
                }
            }
            Console.WriteLine(result);
        }
    }
}
