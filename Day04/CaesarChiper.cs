﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class CaesarChiper
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan kalimat");
 
            string input = Console.ReadLine();
            int shift = Convert.ToInt32(Console.ReadLine());

            int[] alphabet = new int[input.Length]; //input.Length berguna untuk mengukur seberapa panjang lenth tanpa memasukan length di awal
            char[] rotate = new char[input.Length]; // untuk menampung bilangan yang di rotasi

            for (int i = 0; i < input.Length; i++) //sebelum kita if kita bikin dulu inputnya
            {
                alphabet[i] = (int)input[i];
                if (alphabet[i] > 64 && alphabet[i] < 91)
                {
                    // % modulus 26 untuk shift diatas 26
                    alphabet[i] = (65 + (alphabet[i] - 65) + shift) % 26; //(alphabet[i] - 65) ex. alphabet[i] = 67
                }
                else if (alphabet[i] > 96 && alphabet[i] < 123)
                {
                    alphabet[i] = (97 + ((alphabet[i] - 97) + shift) % 26); //%26 untuk mengkontrol supaya selalu di alphabet
                }
                rotate[i] = (char)alphabet[i]; //mengkonvert atau konversion. untuk merubah string
                Console.Write(rotate[i]);
            }
            Console.WriteLine();
        }
    }
}
