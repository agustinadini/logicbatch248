﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class camelCase
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the text");
            string text = Console.ReadLine();

            int count = 1;

            if (char.IsUpper(text[0]))
            {
                Console.WriteLine("Incorrect camel case entered");
            }
            else
            {
                for (int i = 0; i < text.Length; i++)
                {
                    if (char.IsUpper(text[i]))
                        count += 1;
                }

                Console.WriteLine("The result is : " + count);
            }
        }
    }
}
