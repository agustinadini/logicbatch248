﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y") //answer to upper kalo user input huruf kecil akan diubah jadi kapital jd gak error
            {
                Console.WriteLine("Masukan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal) //untuk memilih apa inputan user
                {
                    case 1:
                        Console.WriteLine("Soal camel case");
                        camelCase.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Soal strong password");
                        StrongPassword.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Soal caesar chiper");
                        CaesarChiper.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Soal mars exploration");
                        MarsExploration.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Soal hacker rank in string");
                        HackerRankInString.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Soal pangram");
                        Pangrams.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("Soal separate the number");
                        SeparateTheNumbers.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Soal gemstone");
                        Gemstone.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("Soal making anagrams");
                        MakingAnagrams.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Soal two string");
                        TwoString.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("Soal middle asterisk");
                        MiddleAsterisk.Resolve();
                        break;

                    case 12:
                        Console.WriteLine("Soal palindrome");
                        palinDrome.Resolve();
                        break;
                       

                    default:
                        Console.WriteLine("Soal tidak ditemukan ");
                        break; //untuk menyelesaikan 
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine(); //kalo user input selain Y maka selesai
            }
        }
    }
}
