﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Gemstone
    {
        public static void Resolve()
        {
            Console.WriteLine("Input length"); //masukan banykanya inputan
            int length = int.Parse(Console.ReadLine()); //read length karena habis konvert dari string

            string[] stringArray = new string[length]; //jumlahnya sama seperti length

            int lengthInput = 0;
            while (lengthInput < length) // why while, bcs looping
            {
                Console.WriteLine("Enter the set character " + (lengthInput + 1)); //untuk menampilkan text inputan
                string inputString = Console.ReadLine();

                stringArray[lengthInput] = inputString.ToLower();

                lengthInput++;
            }

            char[] alphabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            //char untuk menampung karakter alfabet

            int sementara = 0; //alat bantu di perulangan
            int count = 0;

            for (int i = 0; i < alphabet.Length; i++)
            {
                for (int j = 0; j < stringArray.Length; j++)
                {
                    if (stringArray[j].Contains(alphabet[i])) //untuk cek satu per satu karakter yang diinput
                    {
                        sementara++; //kalo ada yg sama nambah
                    }
                }

                //untuk mengecek apakah panjangnya sama, mengecek apakah di semua perluangan ada karakter yang sama
                if (sementara == stringArray.Length) // menghitung karakter yang sama yang ada di semua inputan text
                {
                    count++;
                }
                sementara = 0;
            }
            Console.WriteLine(count);
        }
    }
}
