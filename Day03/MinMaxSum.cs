﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class MinMaxSum
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the numbers");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);
            int maximumNumber = numbersArray.Max();
            int minimalNumber = numbersArray.Min();
            int total = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                total += numbersArray[i];
            }
            Console.WriteLine("The result of maximum : " + (total - minimalNumber));
            Console.WriteLine("The result of minimum : " + (total - maximumNumber));
            
        }
    }
}
