﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class VeryBigsum
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the numbers");
            string numbers = Console.ReadLine();

            long[] numbersArray = Utility.ConvertStringToLongArray(numbers);
            long total = 0;

            for (long i = 0; i < numbersArray.Length; i++)
            {
                total += numbersArray[i];
            }

            Console.WriteLine("The total is " + total);
        }
    }
}
