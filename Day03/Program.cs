﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y") //answer to upper kalo user input huruf kecil akan diubah jadi kapital jd gak error
            {
                Console.WriteLine("Masukan nomor soal");

                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal) //untuk memilih apa inputan user
                {
                    case 1:
                        Console.WriteLine("Soal Solve me first");
                        SolveMeFirst.Resolve();
                        break;
                    case 2:
                        Console.WriteLine("Soal Time Conversion");
                        TimeConversion.Resolve();
                        break;
                    case 3:
                        Console.WriteLine("Soal Simple array sum");
                        SimpleArraySum.Resolve();
                        break;
                    case 4:
                        Console.WriteLine("Soal Diagonal Difference");
                        DiagonalDifference.Resolve();
                        break;
                    case 5:
                        Console.WriteLine("Soal Plus Minus");
                        PlusMinus.Resolve();
                        break;
                    case 6:
                        Console.WriteLine("Soal Staircase");
                        Staircase.Resolve();
                        break;
                    case 7:
                        Console.WriteLine("Soal Min Max Sum");
                        MinMaxSum.Resolve();
                        break;
                    case 8:
                        Console.WriteLine("Soal Birthday Cake Candles");
                        BirthdayCakeCandles.Resolve();
                        break;
                    case 9:
                        Console.WriteLine("Soal A Very Big Sum");
                        VeryBigsum.Resolve();
                        break;
                    case 10:
                        Console.WriteLine("Soal Compare The Triplets");
                        CompareTheTriplets.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan ");
                        break; //untuk menyelesaikan 
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine(); //kalo user input selain Y maka selesai
            }
        }

    }
}
