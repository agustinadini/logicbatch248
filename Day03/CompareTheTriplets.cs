﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class CompareTheTriplets
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the first set numbers");
            string numbers1 = Console.ReadLine();

            Console.WriteLine("Enter the second set numbers");
            string numbers2 = Console.ReadLine();

            int[] array1 = Utility.ConvertStringToIntArray(numbers1);
            int[] array2 = Utility.ConvertStringToIntArray(numbers2);
            int firsttotal = 0;
            int secondtotal = 0;

            if (array1.Length != array2.Length)
            {
                Console.WriteLine("You can not enter the set number");
            }
            else
            {
                for (int i = 0; i < array1.Length; i++)
                {
                    if (array1[i] > array2[i])
                    {
                        firsttotal += 1;
                    }
                    else if (array1[i] < array2[i])
                    {
                        secondtotal += 1;
                    }
                }

                Console.WriteLine("Total : " + firsttotal + ":" + secondtotal);
            }
        }
    }
}
