﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class DiagonalDifference
    {
        public static void Resolve()
        {
            //only for input
            Console.WriteLine("Input the length of the matrix");
            int length = int.Parse(Console.ReadLine());
            int diagonal1 = 0;
            int diagonal2 = 0;


            int[,] array2D = new int[length, length];

            for (int i = 0; i < length; i++)
            {
                Console.WriteLine("Enter the " + (i + 1) + " set of numbers");
                string numbers = Console.ReadLine();

                int[] array = Utility.ConvertStringToIntArray(numbers);
                if (array.Length != length)
                {
                    Console.WriteLine("Wrong numbers, try again");
                    i = - 1;
                }
                else
                {
                    for (int j = 0; j < length; j++)
                    {
                        array2D[i, j] = array[j];
                    }
                }
            }

            //Memproses inputan
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        diagonal1 += array2D[i, j];
                    }
                }
            }

            //diagonal atas kanan ke kiri
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (i + j == array2D.GetLength(1) - 1)
                    {
                        diagonal2 += array2D[i, j];
                    }
                }
            }            
            Console.WriteLine();
            Console.WriteLine("Diagonal pertama : " + diagonal1);
            Console.WriteLine("Diagonal Kedua : " + diagonal2);
            Console.WriteLine("Hasilnya : " + Math.Abs(diagonal1 - diagonal2));
        }
    }
}
