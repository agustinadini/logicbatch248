﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class PlusMinus
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the numbers");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);

            float bilanganPositif = 0;
            float bilanganNegatif = 0;
            float bilanganNol = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] > 0)
                {
                    bilanganPositif += 1;
                }
                else if (numbersArray[i] < 0)
                {
                    bilanganNegatif += 1;
                }
                else
                {
                    bilanganNol += 1;
                }
            }
            Console.WriteLine("Bilangan positif : " + bilanganPositif / numbersArray.Length);
            Console.WriteLine("Bilangan negatif : " + bilanganNegatif / numbersArray.Length);
            Console.WriteLine("Bilangan nol : " + bilanganNol / numbersArray.Length);
        }
    }
}
