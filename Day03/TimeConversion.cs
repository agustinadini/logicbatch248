﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class TimeConversion
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the time");
            string time = Console.ReadLine();

            if (!time.Contains("AM") && !time.Contains("PM"))
            {
                Console.Write("You can not input this time");
            }
            else
            {
                try
                {
                    DateTime dateTime = Convert.ToDateTime(time);
                    Console.Write("The time format in 24 hours is  ");
                    Console.WriteLine(dateTime.ToString("HH:mm:ss"));
                }
                catch (Exception e)
                {
                    Console.WriteLine("The time format you entered is wrong");
                }
            }
            Console.WriteLine();
        }
    }
}
