﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01
{
    class Program
    {
        static void Main(string[] args) //method atau function 
        {

            Console.WriteLine("Input nomor soal");
            int nomorSoal = int.Parse(Console.ReadLine());

            if (nomorSoal == 1)
            {
                //Soal BMI
                Console.WriteLine("Masukan tinggi dalam satuan meter");
                double tinggi = double.Parse(Console.ReadLine());

                Console.WriteLine("Masukan berat dalam satuan kg");
                double berat = double.Parse(Console.ReadLine());

                double BMI = berat / (tinggi * tinggi);

                if (BMI < 18.5)
                {
                    Console.WriteLine("Underweight");
                }
                else if (BMI > 25)
                {
                    Console.WriteLine("Overweight");
                }
                else
                {
                    Console.WriteLine("Normal");
                }
            }
            
            else if (nomorSoal == 2)
            {
                //Soal LMS

                Console.WriteLine("Masukan panjang angka");
                int length = int.Parse(Console.ReadLine());

                //Soal 1
                //Output: 1 3 5 7 9 11 13
                int[] angkaGanjilArray = new int[length]; // tanda dari sebuah array [], kalo double array [i, j]
                int angkaGanjil = 1;

                //memasukan value
                for (int i = 0; i < length; i++) // i = indeks
                {
                    angkaGanjilArray[i] = angkaGanjil;
                    angkaGanjil += 2;
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaGanjilArray[i] + " ");
                }

                Console.WriteLine();


                //Soal 2
                //Output: 2 4 6 8 10 12 14
                int[] angkaGenapArray = new int[length];
                int angkaGenap = 2;

                for (int i = 0; i < length; i++)
                {
                    angkaGenapArray[i] = angkaGenap;
                    angkaGenap += 2;
                    Console.Write(angkaGenapArray[i] + " ");
                }

                Console.WriteLine();


                //Soal 3
                //Output: 1 4 7 10 13 16 19
                int[] kelipatanTigaArray = new int[length];
                int kelipatanTiga = 1;

                for (int i = 0; i < length; i++)
                {
                    kelipatanTigaArray[i] = kelipatanTiga;
                    kelipatanTiga += 3;
                    Console.Write(kelipatanTigaArray[i] + " ");
                }
                Console.WriteLine();


                //Soal 4
                //Output: 1 5 9 13 17 21 25
                int[] kelipatanEmpatArray = new int[length];
                int kelipatanEmpat = 1;

                for (int i = 0; i < length; i++)
                {
                    kelipatanEmpatArray[i] = kelipatanEmpat;
                    kelipatanEmpat += 4;
                    Console.Write(kelipatanEmpatArray[i] + " ");
                }
                Console.WriteLine();


                //Soal 5
                //Output: 1 5 * 9 13 * 17
                int soalLima = 1;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 3 == 0)
                    {
                        Console.Write("* ");
                    }
                    else
                    {
                        Console.Write(soalLima + " ");
                        soalLima += 4;
                    }
                }
                Console.WriteLine();


                //Soal 6
                //Output: 1 5 * 13 17 * 25
                int soalEnam = 1;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 3 == 0)
                    {
                        Console.Write("* ");
                    }
                    else
                    {
                        Console.Write((soalEnam) + " ");
                    }
                    soalEnam += 4;
                }
                Console.WriteLine();


                //Saol 7
                //Output: 2 4 8 16 32 64 128
                int soalTujuh = 2;
                for (int i = 0; i < length; i++)
                {
                    Console.Write(soalTujuh + " ");
                    soalTujuh *= 2;
                }
                Console.WriteLine();


                //Soal 8
                //Output: 3 9 27 81 243 729 2187
                int soalDelapan = 3;
                for (int i = 0; i < length; i++)
                {
                    Console.Write(soalDelapan + " ");
                    soalDelapan *= 3;
                }
                Console.WriteLine();


                //Soal 9
                //Output: 4 16 * 64 256 * 1024
                int soalSembilan = 4;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 3 == 0)
                    {
                        Console.Write("* ");
                    }
                    else
                    {
                        Console.Write(soalSembilan + " ");
                        soalSembilan *= 4;
                    }
                }
                Console.WriteLine();


                //Soal 10
                //Output: 3 9 27 XXX 243 729 2187
                int soalSepuluh = 3;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 4 == 0)
                    {
                        Console.Write("XXX ");
                    }
                    else
                    {
                        Console.Write(soalSepuluh + " ");
                    }
                    soalSepuluh *= 3;
                }
                Console.WriteLine();


                //Soal 11
                //Output: 1 1 2 3 5 8 13
                int[] fibonacciArray = new int[length];

                for (int i = 0; i < length; i++)
                {

                    if (i <= 1)
                    {
                        fibonacciArray[i] = 1;
                        Console.Write(fibonacciArray[i] + " ");
                    }
                    else
                    {
                        fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                        Console.Write(fibonacciArray[i] + " ");
                    }
                }
                Console.WriteLine();


                //Soal 12
                //Output: 1 3 5 7 5 3 1
                int[] balikArray = new int[length];

                for (int i = 0; i < length; i++)
                {
                    if (length % 2 == 1)
                    {
                        if (i < 1) //ganjil
                        {
                            balikArray[i] = 1;
                            Console.Write(balikArray[i] + " ");
                        }
                        else if (i <= length / 2)
                        {
                            balikArray[i] = balikArray[i - 1] + 2;
                            Console.Write(balikArray[i] + " ");
                        }
                        else
                        {
                            balikArray[i] = balikArray[i - 1] - 2;
                            Console.Write(balikArray[i] + " ");
                        }
                    }

                    else
                    {
                        if (i < 1) //genap
                        {
                            balikArray[i] = 1;
                            Console.Write(balikArray[i] + " ");
                        }
                        else if (i < length / 2)
                        {
                            balikArray[i] = balikArray[i - 1] + 2;
                            Console.Write(balikArray[i] + " ");
                        }
                        else if (i == (length / 2))
                        {
                            balikArray[i] = balikArray[i - 1];
                            Console.Write(balikArray[i] + " ");
                        }
                        else
                        {
                            balikArray[i] = balikArray[i - 1] - 2;
                            Console.Write(balikArray[i] + " ");
                        }
                    }
                }
                Console.WriteLine();


                //Soal 12 Simple
                //Output: 1 3 5 7 5 3 1
                int[] duaBelasArray = new int[length];
                int kaloGanjil = 1;

                for (int i = 0; i < length; i++)
                {
                    if (length % 2 == 0)
                    {
                        if (i < length / 2)
                        {
                            duaBelasArray[i] = kaloGanjil;
                            duaBelasArray[length - 1 - i] = kaloGanjil;

                            kaloGanjil += 2;
                        }
                    }
                    else
                    {
                        if (i <= length / 2)
                        {
                            duaBelasArray[i] = kaloGanjil;
                            duaBelasArray[length - 1 - i] = kaloGanjil;

                            kaloGanjil += 2;
                        }
                    }
                }
                for (int i = 0; i < length; i++)
                {
                    Console.Write(duaBelasArray[i] + " ");
                }
                Console.WriteLine();


                //Soal 13
                //Output: 1 1 1 3 5 9 17
                int[] tigaBelasArray = new int[length];
                for (int i = 0; i < length; i++)
                {
                    if (i < 3)
                    {
                        tigaBelasArray[i] = 1;
                        Console.Write(tigaBelasArray[i] + " ");
                    }
                    else
                    {
                        tigaBelasArray[i] = tigaBelasArray[i - 1] + tigaBelasArray[i - 2] + tigaBelasArray[i - 3];
                        Console.Write(tigaBelasArray[i] + " ");
                    }
                }
                Console.WriteLine();


                //Soal 14
                // output = 2 3 5 7 11 13 17

                int bilanganPrima = 2;
                int panjangBilanganPrima = 0;

                //while jagain supaya panjangnya tidak lebih dari panjang inputan
                while (panjangBilanganPrima < length)
                {
                    int faktorBilanganPrima = 0;

                    //untuk mencari bilangan prima
                    for (int i = 1; i <= bilanganPrima; i++)
                    {
                        if (bilanganPrima % i == 0)
                        {
                            faktorBilanganPrima += 1;
                        }
                    }

                    //untuk cetak bilangan prima
                    if (faktorBilanganPrima == 2) // ==2 karena bilangan prima itu hanya bisa dibagi dengan 1 dan bilangan itu sendiri
                    {
                        Console.Write(bilanganPrima + " ");
                        panjangBilanganPrima++;
                    }
                    bilanganPrima++;
                }
                Console.WriteLine();


                //Soal 15 fibonacci bolak balik
                //Output: 1 1 2 3 2 1 1
                int[] limaBelasArray = new int[length];
                

                for (int i = 0; i < length; i++)
                {
                    if (length % 2 == 0)
                    {
                        if (i < length / 2)
                        {
                            limaBelasArray[i] = fibonacciArray[i];
                            limaBelasArray[length - 1 - i] = fibonacciArray[i];

                            //limaBelas += 2;
                        }
                    }
                    else
                    {
                        if (i <= length / 2)
                        {
                            limaBelasArray[i] = fibonacciArray[i];
                            limaBelasArray[length - 1 - i] = fibonacciArray[i];

                            //limaBelas += 2;
                        }
                    }
                }
                for (int i = 0; i < length; i++)
                {
                    
                    Console.Write(limaBelasArray[i] + " ");
                }
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Nomor soal tidak ditemukan");
            }
            Console.ReadKey(); //ini buat pause console
        }
    }
}
